package Kho;

public class HangThucPham extends Kho.Document {
    private String nhacungcap;
    private int ngaysanxuat;
    private int ngayhethan;
    public HangThucPham(String mahang, String tenhang, int soluongton, int dongia, String nhacungcap,int ngaysanxuat, int ngayhethan) {
        super(mahang, tenhang, soluongton, dongia);
        this.nhacungcap = nhacungcap;
        this.ngaysanxuat = ngaysanxuat;
        this.ngaysanxuat = ngayhethan;
    }
    public int getNgayhethan() {
        return ngayhethan;
    }

    public void setNgayhethan(int ngayhethan) {
        this.ngayhethan = ngayhethan;
    }

    public String getNhacungcap() {
        return nhacungcap;
    }

    public void setNhacungcap(String nhacungcap) {
        this.nhacungcap = nhacungcap;
    }

    public int getNgaysanxuat() {
        return ngaysanxuat;
    }

    public void setNgaysanxuat(int ngaysanxuat) {
        this.ngaysanxuat = ngaysanxuat;
    }

    @Override
    public String toString() {
        return "Hàng thực phẩm{" +
                " mã hàng= '" + mahang + '\'' +
                ", tên hàng= '" + tenhang + '\'' +
                ", số lượng tồn= '" + soluongton + '\'' +
                ", đơn giá= '" + dongia + '\'' +
                ", nhà cung cấp= '" + nhacungcap + '\'' +
                ", ngày sản xuất= '" + ngaysanxuat + '\'' +
                ", ngày hết hạn= '" + ngayhethan + '\'' +
                '}';
    }

}
