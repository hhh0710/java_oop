package Kho;

import java.util.Scanner;

public class HangDienMay extends Kho.Document {

    private int timebaohanh;
    private int congsuat;

    public HangDienMay(String mahang, String tenhang, int soluongton, int dongia,int timebaohanh, int congsuat) {
        super(mahang, tenhang, soluongton, dongia);
        this.timebaohanh = timebaohanh;
        this.congsuat = congsuat;
    }

    public int getTimebaohanh() {
        return timebaohanh;
    }

    public void setTimebaohanh(int timebaohanh) {
        this.timebaohanh = timebaohanh;
    }

    public int getCongsuat() {
        return congsuat;
    }

    public boolean setCongsuat(int congsuat) {
        if(congsuat > 0){
            this.congsuat = congsuat;
            return true;
        }else {
            System.err.println("Công suất >0");
            return false;
        }
    }

    @Override
    public String toString() {
        return "Hàng điện máy{" +
                " mã hàng= '" + mahang + '\'' +
                ", tên hàng= '" + tenhang + '\'' +
                ", số lượng tồn= '" + soluongton + '\'' +
                ", đơn giá= '" + dongia + '\'' +
                ", time bảo hành= '" + timebaohanh +
                ", công suat= '" + congsuat +
                '}';
    }
}
