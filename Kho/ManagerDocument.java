package Kho;

import java.util.ArrayList;
import java.util.List;

public class ManagerDocument {
    List<Kho.Document> documents;

    public ManagerDocument() {
        this.documents = new ArrayList<>();
    }

    public void addDocument(Kho.Document document) {
        this.documents.add(document);
    }

    public boolean deleteDocument(String id) {
        Kho.Document doc = this.documents.stream()
                .filter(document -> document.getMahang().equals(id))
                .findFirst().orElse(null);
        if (doc == null) {
            return false;
        }
        this.documents.remove(doc);
        return true;
    }

    public void showInfor() {
        this.documents.forEach(documents -> System.out.println(documents.toString()));
    }

    public void searchByHangThucPham() {
        this.documents.stream().filter(doc -> doc instanceof Kho.HangThucPham).forEach(doc -> System.out.println(doc.toString()));
    }

    public void searchByHangSanhSu() {
        this.documents.stream().filter(doc -> doc instanceof Kho.HangSanhSu).forEach(doc -> System.out.println(doc.toString()));
    }

    public void searchByHangDienMay() {
        this.documents.stream().filter(doc -> doc instanceof Kho.HangDienMay).forEach(doc -> System.out.println(doc.toString()));
    }
}
