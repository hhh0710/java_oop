package Kho;

public class HangSanhSu extends Kho.Document {
    private int ngaynhapkho;
    private String nhasanxuat;
    public HangSanhSu(String mahang, String tenhang, int soluongton, int dongia, String nhasanxuat, int ngaynhapkho) {
        super(mahang, tenhang, soluongton, dongia);
        this.nhasanxuat = nhasanxuat;
        this.ngaynhapkho = ngaynhapkho;
    }

    public String getNhasanxuat() {
        return nhasanxuat;
    }

    public void setNhasanxuat(String nhasanxuat) {
        this.nhasanxuat = nhasanxuat;
    }

    public int getNgaynhapkho() {
        return ngaynhapkho;
    }

    public void setNgaynhapkho(int ngaynhapkho) {
        this.ngaynhapkho = ngaynhapkho;
    }

    @Override
    public String toString() {
        return "Hàng sành sứ{" +
                " mã hàng= '" + mahang +'\''+
                ", tên hàng= '" + tenhang + '\'' +
                ", số lượng tồn= '" + soluongton + '\'' +
                ", đơn giá= '" + dongia + '\'' +
                ", nhà sản xuất= '" + nhasanxuat + '\'' +
                ", ngày nhập kho= '" + ngaynhapkho + '\'' +
                '}';
    }
}
