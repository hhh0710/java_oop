package Kho;

public class Document {
    protected String mahang;
    protected String tenhang;
    protected int soluongton;
    protected int dongia;
    public Document(String mahang, String tenhang, int soluongton, int dongia) {
        this.mahang = mahang;
        this.tenhang = tenhang;
        this.soluongton = soluongton;
        this.dongia = dongia;
    }
    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public String getMahang() {
        return mahang;
    }

    public void setMahang(String mahang) {
        this.mahang = mahang;
    }

    public String getTenhang() {
        return tenhang;
    }

    public void setTenhang(String tenhang) {
        this.tenhang = tenhang;
    }

    public int getSoluongton() {
        return soluongton;
    }

    public void setSoluongton(int soluongton) {
        this.soluongton = soluongton;
    }
}
