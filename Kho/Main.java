package Kho;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ManagerDocument managerDocument = new ManagerDocument();


        while (true) {
            System.out.println("1: Nhập thông tin");
            System.out.println("2: Tìm kiếm ");
            System.out.println("3: Hiển thị thông tin");
            System.out.println("4: Xóa thông tin bằng mã hàng");
            System.out.println("5: Thoát");
            String line = scanner.nextLine();
            switch (line) {
                case "1": {
                    System.out.println("a: Thêm hàng thực phẩm");
                    System.out.println("b: Thêm hàng sành sứ");
                    System.out.println("c: Thêm hàng điện máy");
                    String type = scanner.nextLine();
                    switch (type) {
                        case "a": {
                            System.out.print("Mã hàng: ");
                            String mahang = scanner.nextLine();
                            System.out.print("Tên hàng: ");
                            String tenhang = scanner.nextLine();
                            System.out.print("Số lượng tồn: ");
                            int soluongton = Integer.parseInt(scanner.nextLine());
                            System.out.print("Đơn giá: ");
                            int dongia = Integer.parseInt(scanner.nextLine());
                            System.out.print("Nhà cung cấp: ");
                            String nhacungcap = scanner.nextLine();
                            System.out.print("Ngày sản xuất: ");
                            int ngaysanxuat = Integer.parseInt(scanner.nextLine());
                            System.out.print("Ngày hết hạn: ");
                            int ngayhethan = Integer.parseInt(scanner.nextLine());
                            Document hangthucpham = new Kho.HangThucPham(mahang, tenhang, soluongton, dongia, nhacungcap, ngaysanxuat, ngayhethan);
                            managerDocument.addDocument(hangthucpham);
                            System.out.println(hangthucpham.toString());
                            scanner.nextLine();
                            break;

                        }
                        case "b": {
                            System.out.print("Mã hàng: ");
                            String mahang = scanner.nextLine();
                            System.out.print("Tên hàng: ");
                            String tenhang = scanner.nextLine();
                            System.out.print("Số lượng tồn: ");
                            int soluongton = scanner.nextInt();
                            System.out.print("Đơn giá: ");
                            int dongia = scanner.nextInt();
                            System.out.print("Nhà sản xuất ");
                            String nhasanxuat = scanner.nextLine();
                            System.out.print("Ngày nhập kho: ");
                            int ngaynhapkho = scanner.nextInt();
                            Document hangsanhsu = new Kho.HangSanhSu(mahang, tenhang, soluongton, dongia, nhasanxuat, ngaynhapkho);
                            managerDocument.addDocument(hangsanhsu);
                            System.out.println(hangsanhsu.toString());
                            scanner.nextLine();
                            break;
                        }
                        case "c": {
                            System.out.print("Mã hàng: ");
                            String mahang = scanner.nextLine();
                            System.out.print("Tên hàng: ");
                            String tenhang = scanner.nextLine();
                            System.out.print("Số lượng tồn: ");
                            int soluongton = scanner.nextInt();
                            System.out.print("Đơn giá: ");
                            int dongia = scanner.nextInt();
                            System.out.print("Time bảo hành: ");
                            int timebaohanh = scanner.nextInt();
                            System.out.print("Công suất: ");
                            int congsuat = scanner.nextInt();
                            Document hangdienmay = new HangDienMay(mahang, tenhang, soluongton, dongia, timebaohanh, congsuat);
                            managerDocument.addDocument(hangdienmay);
                            System.out.println(hangdienmay.toString());
                            scanner.nextLine();
                            break;
                        }
                        default:
                            break;
                    }
                    break;
                }
                case "2": {
                    System.out.println("a: Tìm hàng thực phẩm");
                    System.out.println("b: Tìm hàng sành sứ");
                    System.out.println("c: Tìm hàng điện máy");
                    String choise = scanner.nextLine();
                    switch (choise) {
                        case "a": {
                            managerDocument.searchByHangThucPham();
                            break;
                        }
                        case "b": {
                            managerDocument.searchByHangSanhSu();
                            break;
                        }
                        case "c":
                            managerDocument.searchByHangDienMay();
                            break;
                        default:
                            System.out.println("Nhập sai");
                            break;
                    }
                    break;
                }
                case "3": {
                    managerDocument.showInfor();
                    break;
                }
                case "4": {
                    System.out.print("Nhập mã hàng để xóa: ");
                    String mahang = scanner.nextLine();
                    System.out.println(managerDocument.deleteDocument(mahang) ? "Thành công" : "Thất bại");
                }
                break;
                case "5": {
                    return;
                }
                default:
                    System.out.println("Nhập sai");
                    continue;
            }

        }
    }

}
